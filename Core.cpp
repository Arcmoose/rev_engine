#include "Core.h"
#include "Test Room/room.h"
#include "Characters/Ankermen/ankermen.h"

#if _DEBUG
	Room tstRoom;
	Ankermen testAnker;
#endif

/* Creating the window for the game to boot into */
void Core::createWindow()
{
	#if _DEBUG
		gWindow = SDL_CreateWindow(
			"REV Engine: Build Number[ ea088b5c ]",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WINDOW_WIDTH,
			WINDOW_HEIGHT,
			SDL_WINDOW_SHOWN
		);
	#endif

	#if (!_DEBUG)
		gWindow = SDL_CreateWindow(
			"Don't be a Hero",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			WINDOW_WIDTH,
			WINDOW_HEIGHT,
			SDL_WINDOW_SHOWN
		);
	#endif

	// Global game renderer, no need to make a debug renderer because it would be useless
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	#if _DEBUG
		loadGameDebug();
	#endif

	#if (!_DEBUG)
		loadGameStandard();
	#endif
}

/* Loading the game in the debugger version */
#if _DEBUG
	void Core::loadGameDebug()
	{
		tstRoom.loadTextures(gRenderer);

		while (!quit)
		{
			// Current renderer being used
			SDL_RenderPresent(gRenderer);

			// Event getter
			SDL_PollEvent(&e);

			// Window events
			windowEvents();

			// Testing room
			tstRoom.loadRoom(gRenderer, e);
		}
		SDL_DestroyRenderer(gRenderer);
		SDL_DestroyWindow(gWindow);
		SDL_Quit();
		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
	}
#endif

/* Loading the game in the release version */
#if (!_DEBUG)
	void Core::loadGameStandard()
	{
		while (!quit)
		{
			// Current renderer being used
			SDL_RenderPresent(gRenderer);

			// Event getter
			SDL_PollEvent(&e);

			windowEvents();
		}
		SDL_DestroyRenderer(gRenderer);
		SDL_DestroyWindow(gWindow);
		SDL_Quit();
		TTF_Quit();
		Mix_Quit();
		IMG_Quit();
	}
#endif

// Getting window button inputs
void Core::windowEvents()
{
	switch (e.type)
	{
		case SDL_KEYDOWN:
			switch (e.key.keysym.sym)
			{
				case SDLK_ESCAPE:
					quit = true;
				break;
			}
		break;

		case SDL_QUIT:
			quit = true;
		break;
	}
}