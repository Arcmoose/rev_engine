#pragma once
#ifndef ANKERMEN_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <iostream>

// ANIMATION MACROS
#define IDLE 0
#define WALK 1
#define BACKWARD_WALK 3
#define DUCKING 4
#define FORWARD_DASH 5
#define BACK_DASH 6
#define JUMP 7
#define BLOCK_FLOOR 8 
#define BLOCK_AIR 9
#define CALL_SHADOW 10

// ATTACK ANIMATION MACROS
#define L_PUNCH 11
#define L_KICK 12
#define H_PUNCH 13
#define H_KICK 14

// Character size macros
#define ANKER_HEIGHT 440
#define ANKER_WIDTH 590 // Default: 1180

class Ankermen
{
	private:
		/* Idle */
		SDL_Texture* Idle;
		SDL_Rect idleRect;

		/* Walking */
		SDL_Texture* Walk;
		SDL_Rect walkRect;

		/* Ducking */
		SDL_Texture* Duck;
		SDL_Rect duckRect;

		/* Backward walking
		SDL_Texture* backwardWalk;
		SDL_Rect backwardRect;
		*/

		/* Back dashing
		SDL_Texture* backDash;
		SDL_Rect backdashRect;
		*/

		/* Forward dashing 
		SDL_Texture* forwardDash;
		SDL_Rect forwarddashRect;
		*/

		/* Jumping */
		SDL_Texture* Jump;
		SDL_Rect jumpRect;

		// SDL_Texture* callShadow;
		// SDL_Texture* lightPunch;
		// SDL_Texture* lightKick;
		// SDL_Texture* heavyPunch;
		// SDL_Texture* heavyKick;
		// SDL_Texture* blockFloor;
		// SDL_Texture* blockAir;

		int currentAnimation = 0;
		int ankerMovX = 20;
		int ankerMovY = 229;
		int movXSpeed = 13;
		int movYSpeed = 50;

		bool onFloor = true;
		bool canJump = false;
	public:
		void loadAnkermen(SDL_Renderer* renderer, SDL_Event event);
		void loadAnimations(SDL_Renderer* renderer);
		void renderAnimations(SDL_Renderer* renderer);
		void ankerController(SDL_Event event);
		void ankerPhys(SDL_Event event);
};

#endif