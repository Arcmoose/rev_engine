#include "ankermen.h"
#include "../Player Controls/playerOne.h"

/* Objects */
PlayerOne anker;

/* Loading Ankerment into the game */
void Ankermen::loadAnkermen(SDL_Renderer* renderer, SDL_Event event)
{
	renderAnimations(renderer);
	ankerPhys(event);
	ankerController(event);
}

/* Loading sprites and animations into RAM/game */
void Ankermen::loadAnimations(SDL_Renderer* renderer)
{
	//#		MOVEMENT ANIMATIONS			#
	Idle = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_idle.png"); // Default idle
	Walk = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_backwardwalk.png"); // Walking
	// backwardWalk = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_walk.png");
	// backDash = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_backdash.png"); // Back dashing
	// forwardDash = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/forwardDash.png"); // Dashing forward
	Jump = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_jump.png"); // Jumping
	// callShadow = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/Shadow.png"); // Calling her shadow

	//#			FIGHTING ANIMATIONS			#
	// lightPunch = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/t_lightpunch.png"); // Light punching
	// lightKick = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/LightKick.png"); // Light kicking
	// heavyPunch = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/HeavyPunch.png"); // Heavy punching
	// heavyKick = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/HeavyKick.png"); // Heavy kick
	// blockAir = IMG_LoadTexture(renderer, "Characters/Ankermen/Sprites/BlockAir.png"); // Blocking in the air
	// blockFloor = IMG_LoadTexture(renderer, "Characters/Ankerment/Sprites/BlockFloor.png"); // Blocking on the floor
}

/* Rendering sprites and animations into the game */
void Ankermen::renderAnimations(SDL_Renderer* renderer)
{
	switch (currentAnimation)
	{
		//#			RENDERING MOVEMENT SPRITES AND ANIMATIONS		#
		case IDLE:
			SDL_RenderCopy(renderer, Idle, NULL, &idleRect);
			idleRect.x = ankerMovX;
			idleRect.y = ankerMovY;
			idleRect.h = ANKER_HEIGHT;
			idleRect.w = ANKER_WIDTH;
		break;

		case WALK:
			SDL_RenderCopy(renderer, Walk, NULL, &walkRect);
			walkRect.x = ankerMovX;
			walkRect.y = ankerMovY;
			walkRect.h = ANKER_HEIGHT;
			walkRect.w = ANKER_WIDTH;
		break;

		/*
		case BACKWARD_WALK:
			SDL_RenderCopy(renderer, backwardWalk, NULL, &backwardRect);
			backwardRect.x = ankerMovX;
			backwardRect.y = ankerMovY;
			backwardRect.h = ANKER_HEIGHT;
			backwardRect.w = ANKER_WIDTH;
		break;
		*/

		/*
		case FORWARD_DASH:
			SDL_RenderCopy(renderer, forwardDash, NULL, NULL);
		break;
		*/

		/*
		case BACK_DASH:
			SDL_RenderCopy(renderer, backDash, NULL, NULL);
		break;
		*/

		case JUMP:
			SDL_RenderCopy(renderer, Jump, NULL, &jumpRect);
			jumpRect.x = ankerMovX;
			jumpRect.y = ankerMovY;
			jumpRect.h = ANKER_HEIGHT;
			jumpRect.w = ANKER_WIDTH;
		break;

		/*
		case DUCKING:
			SDL_RenderCopy(renderer, Duck, NULL, NULL);
		break;

		case CALL_SHADOW:
			SDL_RenderCopy(renderer, callShadow, NULL, NULL);
		break;

		//#			RENDERING ACTION SPRITES AND ANIMATIONS			#
		case L_PUNCH:
			SDL_RenderCopy(renderer, lightPunch, NULL, NULL);
		break;

		case L_KICK:
			SDL_RenderCopy(renderer, lightKick, NULL, NULL);
		break;

		case H_PUNCH:
			SDL_RenderCopy(renderer, heavyPunch, NULL, NULL);
		break;

		case H_KICK:
			SDL_RenderCopy(renderer, heavyKick, NULL, NULL);
		break;

		case BLOCK_AIR:
			SDL_RenderCopy(renderer, blockAir, NULL, NULL);
		break;

		case BLOCK_FLOOR:
			SDL_RenderCopy(renderer, blockFloor, NULL, NULL);
		break;
		*/
	}
}

/* Controller for Ankermen, if visual studio decides to find it... */
void Ankermen::ankerController(SDL_Event event)
{
	// Loading the default player one controls
	anker.defaultPlayerOne(event);

	// Default controls for ankermen
	if (anker.RIGHT_ARROW == true || anker.LEFT_ARROW == true)
	{
		currentAnimation = WALK;
	}
	else if (anker.UP == true || onFloor == false)
	{
		currentAnimation = JUMP;
	}
	else
	{
		currentAnimation = IDLE;
	}
}

/* Testing collision and fighting game physics, will make a default one for all characters to use as a base */
void Ankermen::ankerPhys(SDL_Event event)
{
	// Checking of the player is on the floor
	if (ankerMovY > 229 || ankerMovY == 229)
	{
		onFloor = true;
		canJump = true;
	}

	if (canJump == true && anker.UP == true)
	{
		onFloor = false;

		ankerMovY -= 35;
	}
	else if (onFloor == false)
	{
		ankerMovY += movYSpeed;
	}
}