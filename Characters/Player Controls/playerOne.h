#pragma once
#ifndef PLAYERONE_H

#include <SDL.h>
#include <iostream>

class PlayerOne
{
	public:
		bool UP = false;
		bool DOWN = false;
		bool RIGHT_ARROW = false;
		bool LEFT_ARROW = false;

		bool A = false;
		bool S = false;
		bool D = false;
		bool Z = false;
		bool X = false;
		bool C = false;

		void defaultPlayerOne(SDL_Event event);
};

#endif