#include "playerOne.h"

/*

	Having pre-programmed boolean statements for key getting
	for some reason makes the inputs faster, I have no idea
	why, but it does and doing this is perfect for a fighting
	game. Other fighting game developers using SDL should
	probably do this, if they haven't already.

	Or i'm just really dumb and most fighting game developers
	do this already and I felt special doing it.

*/

void PlayerOne::defaultPlayerOne(SDL_Event event)
{
	switch (event.type)
	{
		// When the key is being pressed
		case SDL_KEYDOWN:
			switch (event.key.keysym.sym)
			{
				case SDLK_UP:
					UP = true;
				break;

				case SDLK_DOWN:
					DOWN = true;
				break;

				case SDLK_RIGHT:
					RIGHT_ARROW = true;
				break;

				case SDLK_LEFT:
					LEFT_ARROW = true;
				break;

				case SDLK_a:
					A = true;
				break;

				case SDLK_s:
					S = true;
				break;

				case SDLK_d:
					D = true;
				break;

				case SDLK_z:
					Z = true;
				break;

				case SDLK_x:
					X = true;
				break;

				case SDLK_c:
					C = false;
				break;
			}
		break;

		// When the key is let up
		case SDL_KEYUP:
			switch (event.key.keysym.sym)
			{
				case SDLK_UP:
					UP = false;
				break;

				case SDLK_DOWN:
					DOWN = false;
				break;

				case SDLK_RIGHT:
					RIGHT_ARROW = false;
				break;

				case SDLK_LEFT:
					LEFT_ARROW = false;
				break;

				case SDLK_a:
					A = false;
				break;

				case SDLK_s:
					S = false;
				break;

				case SDLK_d:
					D = false;
				break;

				case SDLK_z:
					Z = false;
				break;

				case SDLK_x:
					X = false;
				break;

				case SDLK_c:
					C = false;
				break;
			}
		break;
	}
}