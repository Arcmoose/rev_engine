#include "beat.h"
#include "../Player Controls/playerOne.h"

PlayerOne beat;

/* Load Beat into the game */
void Beat::loadBeat(SDL_Renderer* renderer, SDL_Event event)
{
	renderSprites(renderer);
}

/* Loading beat's sprites into RAM/game */
void Beat::loadSprites(SDL_Renderer* renderer)
{
	
}

/* Render Beat's sprites and animations into the game */
void Beat::renderSprites(SDL_Renderer* renderer)
{
	switch (currentAnimation)
	{
		case IDLE:
			SDL_RenderCopy(renderer, Idle, NULL, NULL);
		break;

		case WALK:
			SDL_RenderCopy(renderer, Walk, NULL, NULL);
		break;

		case FORWARD_DASH:
			SDL_RenderCopy(renderer, forwardDash, NULL, NULL);
		break;

		case BACK_DASH:
			SDL_RenderCopy(renderer, backDash, NULL, NULL);
		break;

		case JUMP:
			SDL_RenderCopy(renderer, Jump, NULL, NULL);
		break;

		case BLOCK_FLOOR:
			SDL_RenderCopy(renderer, blockFloor, NULL, NULL);
		break;

		case BLOCK_AIR:
			SDL_RenderCopy(renderer, blockAir, NULL, NULL);
		break;
	}
}

void Beat::beatController(SDL_Event event)
{
	if (beat.RIGHT_ARROW == true)
	{
		currentAnimation = WALK;
	}
	else if (beat.LEFT_ARROW == true)
	{
		currentAnimation = WALK;
	}
	else
	{
		currentAnimation = IDLE;
	}
}