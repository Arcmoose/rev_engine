#pragma once
#ifndef BEAT_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

// ANIMATION MACROS
#define IDLE 0
#define WALK 1
#define FORWARD_DASH 2
#define BACK_DASH 3
#define JUMP 4
#define BLOCK_FLOOR 5 
#define BLOCK_AIR 6

// ATTACK ANIMATION MACROS
#define L_PUNCH 7
#define L_KICK 8
#define H_PUNCH 9
#define H_KICK 10

class Beat
{
	private:
		SDL_Texture* Idle;
		SDL_Texture* Walk;
		SDL_Texture* backDash;
		SDL_Texture* forwardDash;
		SDL_Texture* Jump;
		SDL_Texture* callShadow;
		SDL_Texture* lightPunch;
		SDL_Texture* lightKick;
		SDL_Texture* heavyPunch;
		SDL_Texture* heavyKick;
		SDL_Texture* blockFloor;
		SDL_Texture* blockAir;

		int currentAnimation = 0;
	public:
		void loadBeat(SDL_Renderer * renderer, SDL_Event event);
		void loadSprites(SDL_Renderer * renderer);
		void renderSprites(SDL_Renderer * renderer);
		void beatController(SDL_Event event);
};

#endif