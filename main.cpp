#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#include "Core.h"

/* Objects */
Core init;

int main(int argc, char* argv[])
{
	// Staring up the basic SDL libs
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO);
	IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG);
	Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG);
	TTF_Init();

	// Loading the window
	init.createWindow();

	return 0;
}