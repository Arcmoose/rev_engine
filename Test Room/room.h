#pragma once
#ifndef ROOM_H

#include <SDL.h>
#include <SDL_image.h>

class Room
{
	private:
		SDL_Texture* baseRoom;
		SDL_Rect baseRoomRect;

		int roomCamX = -300;
		int roomCamY = -300;
	public:
		void loadRoom(SDL_Renderer* renderer, SDL_Event event);
		void loadTextures(SDL_Renderer* renderer);
		void renderRoom(SDL_Renderer* renderer);
};

#endif