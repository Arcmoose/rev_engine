#pragma once
#ifndef CORE_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>

#define WINDOW_HEIGHT 720
#define WINDOW_WIDTH 1280

class Core
{
	private:
		SDL_Window* gWindow;
		SDL_Renderer* gRenderer;
		SDL_Event e;

		bool quit = false;
	public:
		void createWindow();
		void loadGameDebug();
		void loadGameStandard();
		void windowEvents();
};

#endif // !CORE_H
